#!/usr/bin/env python

import os
import sys
import argparse
from collections import defaultdict
import string
import shlex
import time
import subprocess
import glob
import random
import shutil
import re

# ------------------------------------------------------------------------------
# create the call for one sample
# ------------------------------------------------------------------------------
def create_single_call(directory, output_dir, sample_id, type_job_scheduler,save_bam_file,forward_id,rev_id,single_id,bam_input,cutoff_val,type_output,taxonomic_level,print_raw_reads,db,min_len_align_length,save_mgc_table,print_only_specI):
	motu_cmd = "motus profile -t 10 "


	motu_cmd = motu_cmd + " -g "+cutoff_val+" "
	motu_cmd = motu_cmd + " -l "+str(min_len_align_length)+" "
	motu_cmd = motu_cmd + " -y "+type_output + " "
	motu_cmd = motu_cmd + " -k "+taxonomic_level + " "

	#if db != 'aug.cen':
		#motu_cmd = motu_cmd + " -db "+db+ " "


	if save_bam_file != "":
		motu_cmd = motu_cmd + " -I "+directory+"/"+save_bam_file+" "

	if save_mgc_table != "":
		motu_cmd = motu_cmd + " -o "+output_dir+sample_id+".mgc "

	if print_raw_reads:
		motu_cmd = motu_cmd + " -w "

	if print_only_specI:
		motu_cmd = motu_cmd + " -e "


	files = os.listdir(directory)
	if bam_input != "":
		motu_cmd = motu_cmd + " -i "+directory+"/"+bam_input
	else:
		if "mapped_reads.bam" in files: files.remove("mapped_reads.bam") # trick to dont take into account this file when present
		if "mapped_reads_nr.bam" in files: files.remove("mapped_reads_nr.bam") # trick to dont take into account this file when present
		if "mapped_reads_bowtie2.bz2" in files: files.remove("mapped_reads_bowtie2.bz2") # trick to dont take into account this file when present
		# if there is only one, we take it as it is
		if len(files) == 1:
			motu_cmd = motu_cmd + " -s "+directory+"/"+files[0]
		# if there are 2 files, it can be S,S or F,R
		if len(files) == 2:
			# both single
			if (re.findall(single_id, files[0])):
				motu_cmd = motu_cmd + " -s "+directory+"/"+files[0]
				if (re.findall(single_id, files[1])):
					motu_cmd = motu_cmd + ","+directory+"/"+files[1]
				else:
					sys.stderr.write("Warning. sample "+sample_id+": match only one file out of two")
			# for and rev
			else:
				count = 0
				for file_fastq in files:
					if (re.findall(forward_id, file_fastq)):
						motu_cmd = motu_cmd + " -f "+directory+"/"+file_fastq
						count = count + 1
					if (re.findall(rev_id, file_fastq)):
						motu_cmd = motu_cmd + " -r "+directory+"/"+file_fastq
						count = count + 1
				if count != 2:
					sys.stderr.write("Warning. sample "+sample_id+": found two files, but were not all matched")

		# if there are 3 files, it can be S,S,S or F,R,S
		if len(files) == 3:
			# S,S,S
			count = 0
			if (re.findall(single_id, files[0])):
				motu_cmd = motu_cmd + " -s "+directory+"/"+files[0]
				for file_fastq in files[1:]:
					if (re.findall(forward_id, file_fastq)):
						motu_cmd = motu_cmd + " -f "+directory+"/"+file_fastq
					if (re.findall(rev_id, file_fastq)):
						motu_cmd = motu_cmd + " -r "+directory+"/"+file_fastq
					if (re.findall(single_id, file_fastq)):
						motu_cmd = motu_cmd + ","+directory+"/"+file_fastq

				# for and rev and singleReads
			else:
				for file_fastq in files:
					if (re.findall(forward_id, file_fastq)):
						motu_cmd = motu_cmd + " -f "+directory+"/"+file_fastq
					if (re.findall(rev_id, file_fastq)):
						motu_cmd = motu_cmd + " -r "+directory+"/"+file_fastq
					if (re.findall(single_id, file_fastq)):
						motu_cmd = motu_cmd + " -s "+directory+"/"+file_fastq
		# if there are more than 3 files
		if len(files) > 3: # we look for the word lane
			# find all the lanes that there are
			lane_list = list()
			for i in files:
				m = re.search('lane[0-9]*\.', i)
				if m:
					lane_list.append(m.group(0))
			lanes = list(set(lane_list))
			# create the proper list
			single = ""
			forw = ""
			reve = ""
			for lane in lanes:
				for i in files:
					if (re.findall(lane, i)):
						if (re.findall(forward_id, i)):
							forw=forw+directory+"/"+i+","
						if (re.findall(rev_id, i)):
							reve=reve+directory+"/"+i+","
						if (re.findall(single_id, i)):
							single=single+directory+"/"+i+","
			single = single[0:-1]
			forw = forw[0:-1]
			reve=reve[0:-1]
			# add to cmd
			motu_cmd = motu_cmd + " -f "+forw
			motu_cmd = motu_cmd + " -r "+reve
			motu_cmd = motu_cmd + " -s "+single

	# add rest of the cmd

	motu_cmd = motu_cmd + " -o "+output_dir+sample_id+".taxonomy "
	motu_cmd = motu_cmd + " -n "+sample_id

	# wrap in the scheduler
	if type_job_scheduler == "LSF":
		motu_cmd = "bsub -n 1 -q scb -M 12000 -o job_output_"+sample_id+".txt -e job_error_"+sample_id+".txt \""+motu_cmd+" \""

	if type_job_scheduler == "nohup":
		motu_cmd = motu_cmd + " 2> "+output_dir+sample_id+".err "
		motu_cmd = "nohup " + motu_cmd + " &"

	if type_job_scheduler != "slurm":
		print(motu_cmd)
	else:
		f = open("temp/"+sample_id+"_slurm.sh","w")
		f.write("#!/bin/bash\n")
		f.write("#SBATCH -A zeller\n")
		if bam_input != "":
			f.write("#SBATCH -p 1day\n")
			f.write("#SBATCH --mem 10G\n")
			f.write("#SBATCH -t 1-00:00\n")
		else:
			f.write("#SBATCH -p 1day\n")
			f.write("#SBATCH -t 1-00:00\n")
			f.write("#SBATCH --mem 15G\n")
			f.write("#SBATCH -n 3\n")
			f.write("#SBATCH --cpus-per-task=3\n")
		#f.write("#SBATCH -n 35\n")
		f.write("#SBATCH -o "+output_dir+sample_id+".out \n")
		f.write("#SBATCH -e "+output_dir+sample_id+".err \n\n")
		f.write("module load BWA \n\n")
		f.write("module load SAMtools \n\n")
		f.write(motu_cmd+" \n")
		f.close()

# ------------------------------------------------------------------------------
# create calls for the motu profile
# ------------------------------------------------------------------------------
def create_calls(sample_list, directory, output_dir, type_job_scheduler,save_bam_file,forward_id,rev_id,single_id,bam_input,cutoff_val,type_output,taxonomic_level,print_raw_reads,db,min_len_align_length,save_mgc_table,print_only_specI):
	if type_job_scheduler == "slurm":
		#create temp dir with the scripts for slurm
		if not os.path.exists("temp"):
			os.makedirs("temp")

	# read content of sample_list
	for sample in open(sample_list):
		sample = sample.strip()
		sample_dir = directory+"/"+sample
		# we assume that inside the directory there can be one directory,
		# but all the files should be togheter
		files = os.listdir(sample_dir)
		if len(files) == 1:
			path_file = sample_dir+"/"+files[0]
			if os.path.isdir(path_file):
				files = os.listdir(path_file)
				sample_dir = path_file
		# check that they are not directory
		for file_fastq in files:
			if os.path.isdir(sample_dir+"/"+file_fastq):
				sys.stderr.write("Error. "+sample_dir+"/"+file_fastq+" is a directory.\n")
				sys.exit()

		if type_job_scheduler == "slurm":
			print("sbatch temp/"+sample+"_slurm.sh")

		create_single_call(sample_dir, output_dir, sample, type_job_scheduler,save_bam_file,forward_id,rev_id,single_id,bam_input,cutoff_val,type_output,taxonomic_level,print_raw_reads,db,min_len_align_length,save_mgc_table,print_only_specI)

# ------------------------------------------------------------------------------
# MAIN
# ------------------------------------------------------------------------------
def main(argv=None):

	#----------------------------- input parameters ----------------------------
	parser = argparse.ArgumentParser(description='This program creates a file with the call for motu profile for many samples', add_help = True)
	parser.add_argument('sample_list', action="store", help='name of the file with the list of samples')
	parser.add_argument('output_dir', action="store", help='path of the output directory')
	parser.add_argument('-f', action="store", default = ".pair.1.", dest='forward_id', help='how you recognize the forward file')
	parser.add_argument('-r', action="store", default = ".pair.2.", dest='rev_id', help='how you recognize the rev file')
	parser.add_argument('-s', action="store", default = ".single.", dest='single_id', help='how you recognize the single file')
	parser.add_argument('-i', action="store", default = "", dest='bam_input', help='name of the bam file that you want to give as input')
	parser.add_argument('-g', action="store", default = "2", dest='cutoff_val', help='value of the cutoff')
	parser.add_argument('-y', action="store", dest='type_output', default='insert.scaled_counts', help='type of output that you want to print',choices=['base.coverage', 'insert.raw_counts', 'insert.scaled_counts'])
	parser.add_argument('--directory','-d', action="store", default = "", dest='directory', help='directory where the sample directories are')
	parser.add_argument('--save_bam_file', action="store", default = "", dest='save_bam_file', help='if save the bam file, put the name')
	parser.add_argument('-M', action="store", default = "", dest='save_mgc_table', help='if save the mgc table, put the name')
	parser.add_argument('--type', action="store",dest='type_job_scheduler', default="None", help='type of job scheduler', choices=["None","LSF","nohup","slurm"])
	parser.add_argument('-k', action="store", default = "mOTU", dest='taxonomic_level', help='Taxonomic level for the profiling',choices=['kingdom', 'phylum', 'class', 'order', 'family', 'genus', 'mOTU'])
	parser.add_argument('-w', action='store_true', default=False, dest='print_raw_reads', help='print result as relative abundance instead of raw reads')
	parser.add_argument('-db', action="store", default = "nr", dest='db', help='database of marker genes',choices=['nr', 'aug.cen', 'cen'])
	parser.add_argument('-l', type=int, action="store", dest='min_len_align_length', default=75, help='Minimum length of the alignment')
	parser.add_argument('-e', action='store_true', default=False, dest='print_only_specI', help='print only specI and metaOTU goes to -1')
	args = parser.parse_args()

	#----------------------- check the directory -------------------------------
	directory = args.directory
	if directory == "": #we assume that the directory of the files is the one where the sample_list is contained
		path_array = args.sample_list.split("/")
		directory = "/".join(path_array[0:-1])

	if args.output_dir != "":
		if (args.output_dir [-1]!="/"):
			args.output_dir = args.output_dir + "/"

	# call the function that creates the motu profile executions
	create_calls(args.sample_list,directory,args.output_dir,args.type_job_scheduler,args.save_bam_file,args.forward_id,args.rev_id,args.single_id,args.bam_input,args.cutoff_val,args.type_output,args.taxonomic_level,args.print_raw_reads,args.db,args.min_len_align_length,args.save_mgc_table, args.print_only_specI)

	return 0        # success

#-------------------------------- run main -------------------------------------
if __name__ == '__main__':
	status = main()
	sys.exit(status)
